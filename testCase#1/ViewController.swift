//
//  ViewController.swift
//  testCase#1
//
//  Created by Alina Taimasova on 07.06.2018.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var wordOne: UITextField!
    @IBOutlet weak var wordTwo: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var result = String()
    var counter: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
        
    func compare() {
        let wordOneArray = Array(wordOne.text!)
        let wordTwoArray = Array(wordTwo.text!)
        
        // Inserting option
        firstLoop: for k in wordOneArray {
            secondLoop: for j in wordTwoArray {
                if k == j && wordTwoArray.index(of: j)! >= wordOneArray.index(of: k)! && wordTwoArray.index(of: j)! <= wordTwoArray.count && wordOneArray.index(of: k)! <= wordOneArray.count {
                    
                    continue firstLoop
                }
            }
            if wordOneArray.count < wordTwoArray.count {
                
                result += "i"
            }
        }
        
        // Swaping option
        firstLoop: for k in wordOneArray {
            secondLoop: for j in wordTwoArray {
                if k == j {
                    continue firstLoop
                }
            }
            result += "s"
        }
        
        // Deleting Option
        firstLoop: for k in wordOneArray {
            secondLoop: for j in wordTwoArray {
                
                if wordOneArray.index(of: k)! > wordTwoArray.index(of: j)! && wordTwoArray.index(of: j)! < wordTwoArray.count && wordOneArray.index(of: k)! < wordOneArray.count {
                    
                    continue firstLoop
                }
            }
            if wordOneArray.count > wordTwoArray.count {
                result += "d"
            }
        }
    }
  
    @IBAction func butToCompare(_ sender: Any) {
        result = ""
        compare()
        if wordOne.text == nil || wordTwo.text == nil {
            resultLabel.text = "Please write your word :)"
        } else {
            resultLabel.text = result
        }
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Custom Keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
}

